import numpy as np
from torchvision.datasets import MNIST as _MNIST

class MNIST(_MNIST):
    def __getitem__(self, index: int):
        item = super().__getitem__(index)
        image, label = item
        image = np.array(image, dtype=np.float32).flatten() / 255
        label = np.eye(10)[label].astype(int)

        return {
            "rgb": image,
            "class": label
        }

    def collate_fn(self, x):
        return {k: np.array([y[k] for y in x], dtype=x[0][k].dtype) for k in x[0].keys()}
