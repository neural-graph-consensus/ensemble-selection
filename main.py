import yaml
import torch as tr
from ngclib_cv.nodes import RGB as _RGB
from ngclib.models.ngc_v1 import NGCV1
from ngclib.trainer.edge_trainer_sequential import EdgeTrainerSequential
from ngclib.readers import CloneDimReader, GraphReader
from overrides import overrides
from nwgraph import Graph
from nwgraph.utils import NodeCriterionType
from nwgraph.node import VectorNode, MapNode
from nwutils.pytorch_lightning import PLModule
from torch import nn
from torch.nn import functional as F
from pathlib import Path
from torch.utils.data import DataLoader
from nwutils.torch import tr_get_data
from torchmetrics.functional import accuracy

from reader import MNIST

class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(28*28, 100)
        self.fc2 = nn.Linear(100, 10)

    def forward(self, x):
        y1 = F.relu(self.fc1(x))
        y2 = F.softmax(self.fc2(y1), dim=-1)
        return y2

class ClassNode(VectorNode):
    def loss_fn(y, gt):
        gt = gt.type(tr.float)
        loss = F.cross_entropy(y, gt)
        return loss

    def get_encoder(self, output_node) -> nn.Module:
        return nn.Identity()

    def get_decoder(self, input_node) -> nn.Module:
        return nn.Identity()

    @overrides
    def get_node_criterion(self) -> NodeCriterionType:
        return ClassNode.loss_fn

    def get_node_metrics(self):
        return {"accuracy": accuracy}

class RGB(_RGB):
    def __init__(self, name: str, hyper_parameters: dict = None):
        MapNode.__init__(self, name, num_dims=28*28, hyper_parameters=hyper_parameters)

    def get_encoder(self, output_node) -> nn.Module:
        return Model()

    def get_decoder(self, input_node) -> nn.Module:
        return nn.Identity()

def main():
    train_reader = MNIST("/home/mihai/datasets/MNIST/")
    val_reader = MNIST("/home/mihai/datasets/MNIST/", train=False)
    graph_cfg = yaml.safe_load(open("graph.yaml", "r"))
    train_cfg = yaml.safe_load(open("train.yaml", "r"))
    nodes = [RGB("rgb"), RGB("rgb2"), RGB("rgb3"), RGB("rgb4"), ClassNode("class", num_dims=10)]
    model = NGCV1(nodes=nodes, cfg=graph_cfg)
    print(PLModule(model).summary())

    train_reader = GraphReader(CloneDimReader(train_reader, dims={"rgb": ["rgb", "rgb2", "rgb3", "rgb4"]}),
                                              data_dims=["rgb", "rgb2", "rgb3", "rgb4"],
                                              label_dims=["class"])
    val_reader = GraphReader(CloneDimReader(val_reader, dims={"rgb": ["rgb", "rgb2", "rgb3", "rgb4"]}),
                                            data_dims=["rgb", "rgb2", "rgb3", "rgb4"],
                                            label_dims=["class"])
    trainer = EdgeTrainerSequential(model.edges, train_reader, edge_validation_readers=val_reader)
    trainer.run(train_cfg, debug=False)
    exit()

    model.load_all_edges(Path.cwd())
    val_loader = DataLoader(val_reader, batch_size=10000, collate_fn=MNIST.collate_fn)
    item = tr_get_data(next(iter(val_loader)))
    data, labels = item["data"], item["labels"]
    with tr.no_grad():
        y = Graph.forward(model, tr_get_data(data), num_iterations=1)

    output = tuple(y["class"])[0].output
    inputs = tuple(y["class"])[0].input

    out_loss = F.cross_entropy(output, labels)
    in_losses = [float(F.cross_entropy(x, labels)) for x in inputs]
    print(f"In losses {[round(x, 4) for x in in_losses]}. Avg: {sum(in_losses)/len(in_losses):.4f}."
          f"Out loss: {out_loss:.4f}")

    out_acc = accuracy(output, labels)
    in_acc = [float(accuracy(x, labels)) for x in inputs]
    print(f"In acc {[round(x, 4) for x in in_acc]}. Avg: {sum(in_acc)/len(in_acc):.4f}. Out acc: {out_acc:.4f}")

if __name__ == "__main__":
    main()
