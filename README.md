# Ensemble Selection

This repository holds the implementation of various simple identical ensembles with different seeds or architectures of a single input node and a single output node. For graph architectures, go to [link](https://gitlab.com/neural-graph-consensus/ngc-video-scene-understanding).

## Supported datastes

- MNIST (WIP)
- Dronescapes/Slanic (WIP)
- CityScapes (TODO)

## What this ensembles are

- Simple `input node -> target node` identical single link edges of different initialization seeds or model architectures, using various merging functions (mean, median, pretrained network, pretrained kernels etc.)

- Mostly computer vision tasks: `RGB -> Depth`, `RGB -> Semantic`, `RGB -> classifiation class` etc.

- Just one NGC iteration and a selection

- an optional distillation at the end for semisupervised learning or graph-in-graph (TODO).

## What these ensembles aren't
- NGC edges (residuals with multiple input nodes, two hops, cycles, multi iteration ensembles etc.)

